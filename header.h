#ifndef _HEADER_H_
#define _HEADER_H_

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <time.h>

#include <pthread.h>
#include <semaphore.h>

#include "macro.h"
#include "new_struct.h"

// shared memory
shm_struct* init_shm_mem(char* origin, bool is_producer)
{
    int fd;
    if (is_producer)
    {
        shm_unlink(SHM_KEY);
        fd = shm_open(SHM_KEY, O_EXCL | O_CREAT | O_RDWR, S_IREAD | S_IWRITE);
    }
    else
    {
        fd = shm_open(SHM_KEY, O_CREAT | O_RDWR, S_IREAD | S_IWRITE);
    }
    if (fd == -1)
    {
        fprintf(stderr, "%s shm_open failed. Error code: %s\n", origin,
                strerror(errno));
    }
    if (ftruncate(fd, sizeof(shm_struct)) == -1)
    {
        fprintf(stderr, "%s ftruncate failed. Error code: %s\n", origin,
                strerror(errno));
    }
    shm_struct* memory_ptr = mmap(NULL, sizeof(shm_struct),
                    PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (memory_ptr == MAP_FAILED)
    {
        fprintf(stderr, "%s mmap failed. Error code: %s\n", origin,
                strerror(errno));
    }
    memory_ptr->cont_sym = '*';
    // set 1st char to * to mark continuation
    printf("%s All Ready\n", origin);
    return memory_ptr;
}

int end_shm_mem(char* origin)
{
    int val = shm_unlink(SHM_KEY);
    if (val == -1)
    {
        fprintf(stderr, "%s shm_unlink failed. Error code: %s\n", origin,
                strerror(errno));
    }
    return val;
}

int getInput(char* buffer, int len)
{
    char format[32];
    if (len == 0)
    {
        return 0;
    }
    snprintf(format, sizeof(format), "%%%ds", (int)(len-1));
    return scanf(format, buffer);
/*
    FILE *input;
    input = fopen("input", "r");
    fgets(buffer, len, (FILE*)input);
    fclose(input);
    return 0;
*/
}

void CPUconsume(int factor)
{
    int a = 0, b = 1;
    if (factor < 0)
    {
        return;
    }
    int j;
    unsigned int i;
    for (j = 0; j < factor; ++j)
    {
        for (i = 0; i < (unsigned int)(0 - 1); ++i )
        {
            a = b + 1;
            b = b | a >> 1;
        }
    }
}
#endif  // _HEADER_H_
