# README #
## What
Code for multiple producers and multiple consumers, with limited buffer.

## Build
On a POSIX system, with GCC (C11) compiler, in a terminal, call:
```sh
$ make all
```
## Run
In 2 different terminals, run
```sh
$ mother
$ server
```
Mother also takes in user input
## Contributors
* Kunal Tyagi: 120010006
* Aniket Deshmukh: 12d170008
