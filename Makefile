.phony: clean
.phony: all

all: mother server


pnum=4
qnum=8
tnum=4
msg_size=10

CFLAGS=-g
LIBFLAGS = -pthread -lrt -Wall -Wextra
INP_FLAGS= -D_XOPEN_SOURCE -D_BSD_SOURCE -DPNUM=${pnum} -DQNUM=${qnum} -DTNUM=${tnum} -DMSG_SIZE=${msg_size}

headers=$(wildcard *.h)

mother: producer.c ${headers}
	gcc ${CFLAGS} -o mother producer.c ${INP_FLAGS} ${LIBFLAGS}

server: consumer.c ${headers}
	gcc ${CFLAGS} -o server consumer.c ${INP_FLAGS} ${LIBFLAGS}

Cons2: my_consumer.c ${headers}
	gcc ${CFLAGS} -o Cons2 my_consumer.c ${INP_FLAGS} ${LIBFLAGS}

clean:
	rm server mother
