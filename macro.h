#ifndef _MACRO_H_
#define _MACRO_H_

#define SEC_FACTOR 1000.0
// macros, via Makefile :D
#ifndef SLEEP_TIME
#define SLEEP_TIME 2.5
#endif

#ifndef PNUM
#define PNUM 10
#endif

#ifndef QNUM
#define QNUM 8
#endif

#ifndef TNUM
#define TNUM 5
#endif

#ifndef MSG_SIZE
#define MSG_SIZE 10
#endif

#ifndef SHM_KEY
#define SHM_KEY "/120010006"
#endif

#ifndef SAFETY
#define SAFETY 2
#endif

#ifndef SHM_SIZE
#define SHM_SIZE sizeof(shm_struct)
#endif

#ifndef SHM_FLAG  // 0 for octal
#define SHM_FLAG IPC_CREAT | 0666
#endif

#endif  // _MACRO_H_
