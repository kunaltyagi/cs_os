#include "header.h"

void* query_maker(void*);

typedef struct produce_args produce_args;
struct produce_args
{
    shm_struct* mem;
};

int a = 0;

int main()  // int argc, char** argv)
{
    printf("[Producer] All Begins: %d\n", getpid());
    sleep(1);
    shm_struct* shm_mem = init_shm_mem("[Producer]", true);

    sem_t* thread_num;
    /* thread_num = mmap(NULL, sizeof(*thread_num), PROT_READ | PROT_WRITE, */
    /*                             MAP_SHARED | MAP_ANONYMOUS, -1, 0); */
    /* sem_init(thread_num, 0, PNUM); */
    thread_num = sem_open ("pnum", O_CREAT | O_EXCL, 0644, PNUM);
    sem_unlink ("pnum");

    init_shm_struct(shm_mem);

    pthread_mutex_unlock(&(shm_mem->lock));
    pid_t pid;
    while (shm_mem->cont_sym == '*')
    {
        // Produce
        //printf("[Producer] Semaphore init\n");
        sem_wait(thread_num);
        int value;
        sem_getvalue(thread_num, &value);
        //printf("[Producer] About to fork: %d\n", value);
        pid = fork();
        if (pid == 0)
        {
            printf("[Producer] Forked\n\n");
            produce_args* args = (produce_args*)malloc(sizeof(produce_args));
            args->mem = shm_mem;
            query_maker(args);
            sem_post(thread_num);
            sem_getvalue(thread_num, &value);
            printf("[Producer] End of fork: %d\n", value);
            break;
        }
        else
        {
            printf("[Producer] Forked: %d\n", pid);
            /* usleep(SLEEP_TIME * SEC_FACTOR); */
        }
        // 0. Create a thread (if possible)
        // 1. Take in data
        // 2. If shared_mem space is left, store else, block & wait
        // 3. Consume some CPU time
        // 4. Wait via Kernel request
        // 5. End
        printf("[Producer] Looping\n");
    }
    if (pid != 0)
    {
        munmap(thread_num, sizeof(*thread_num));
        sem_destroy(thread_num);
        end_shm_mem("[Producer]");
        printf("[Producer] All Ends\n");
    }
    else
    {
        printf("[Producer] Child Ends\n");
    }
    return 0;
}

void* query_maker(void* message)
{
    produce_args* args = (produce_args*)message;
    shm_struct* mem = args->mem;
    Queue* cq = &(mem->cq);
    char origin[128];
    snprintf(origin, sizeof(origin), "[Producer:%d] ", getpid());

    /* char* msg = (char*)malloc(sizeof(char)*(MSG_SIZE + 1)); */
    //printf("%sWaiting for user input\n", origin);

    /*
    //Manual input
    char msg[MSG_SIZE + 1];
    getInput(msg, MSG_SIZE + 1);
    */

    /*if (strncmp(msg, "q", 1) == 0)
    {
        mem->cont_sym = '?';
        goto CLEANUP;
    }
    if (mem->cont_sym == '?')
    {
        goto CLEANUP;
    }*/

    //Random query
    char msg[MSG_SIZE + 1] = "Test 0";
    msg[5] = 65 + getpid()%17;
    printf("%sGenerated query %s\n", origin, msg); 
    sleep(1);

    struct timeval tv;
    char buffer[30];
    time_t curtime;

    //printf("%sMessage received: %s\n", origin, msg);
    pthread_mutex_lock(&(mem->lock));
    //printf("%sLocked buffer\n", origin);
    while (is_full(cq) == true)
    {
        printf("%sBuffer full. Waiting...\n", origin);
        pthread_cond_wait(&(mem->buf_empty), &(mem->lock));
	printf("%sSignal recieved\n", origin);
    }
    //pthread_mutex_unlock(&(mem->prod_lock)); */
    /* printf("%sLocking buffer\n", origin); */
    /* pthread_mutex_lock(&(mem->lock)); */
    //printf("%sGetting pointer to insert\n", origin);
    Buffer* next = cq_get_next(mem);
    //printf("%sEditing buffer\n", origin);
    memcpy(next->msg, msg, MSG_SIZE + 1);
    //printf("%sInserting into buffer\n", origin);
    cq_push_back(cq, next);
    pthread_mutex_unlock(&(mem->lock));
    gettimeofday(&tv, NULL);
    curtime = tv.tv_sec;
    strftime(buffer,30,"%T",localtime(&curtime));
    fprintf(stderr,"%sMessage: %s, Time :%s\n", origin, msg, buffer);
    //printf("%sUnlocking buffer\n", origin);

    pthread_mutex_lock(&(mem->lock));
    printf("%sSending buffer full signal\n", origin);
    pthread_cond_signal(&(mem->buf_full));
    pthread_mutex_unlock(&(mem->lock));


    printf("%sSignal sent\n", origin);
    pthread_mutex_lock(&(next->lock));
    printf("%sWaiting for completion\n", origin);
    pthread_cond_wait(&(next->buf_process), &(next->lock));
    printf("%sConsume CPU time, sleep and then exit\n\n", origin);
    pthread_mutex_unlock(&(next->lock));

    // consume CPU time
    int i;
    for(i=0; i<1000; i++);
	
    // non blocking wait
    sleep(1.5*SLEEP_TIME);
    sleep(2);
    // kernel request

/* CLEANUP: */
    /* free(msg); */
    free(message);
    return NULL;
}
