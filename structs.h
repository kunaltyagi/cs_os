#ifndef _STRUCTS_H_
#define _STRUCTS_H_

// structs for easier life
typedef struct Buffer Buffer;
struct Buffer
{
    char msg[MSG_SIZE + 1];
    Buffer* next;
    pthread_cond_t buf_process;
    pthread_mutex_t lock;
};
/* const Buffer BUF_EMPTY = { "\0",  NULL, {}, {}}; */

typedef struct Queue Queue;
struct Queue
{
    unsigned int elements;
    Buffer* head;
    Buffer* tail;
};
/* const Queue Q_EMPTY = {0, NULL, NULL}; */

typedef struct shm_struct shm_struct;
struct shm_struct
{
    char cont_sym;
    Queue cq;
    Buffer buffers[QNUM];
    pthread_mutex_t lock;
    pthread_cond_t buf_empty, buf_full;
};
/* const shm_struct MEM_EMPTY = { {}, {}, {}, '\0', {0, NULL, NULL}, {}}; */

bool init_shm_struct(shm_struct* ptr)
{
    if (ptr == NULL)
    {
        return false;
    }
    ptr->cont_sym = '*';

    ptr->cq.elements = 0;
    ptr->cq.head = NULL;
    ptr->cq.tail = NULL;
    for (int i = 0; i < QNUM; ++i)
    {
        ptr->buffers[i].msg[0] = '\0';
        /* ptr->buffers[i].next = NULL; */
        pthread_mutex_init(&(ptr->buffers[i].lock), NULL);
        pthread_cond_init(&(ptr->buffers[i].buf_process), NULL);
        ptr->buffers[i].next = &(ptr->buffers[(i + 1) % QNUM]);
    }
    pthread_mutex_init(&(ptr->lock), NULL);
    pthread_cond_init(&(ptr->buf_empty), NULL);
    pthread_cond_init(&(ptr->buf_full), NULL);
    return true;
}

void q_print(Queue* q)
{
    printf("!~~");
    for (Buffer* current = q->head; current != NULL; current = current->next)
    {
        printf("> %s --", current->msg);
    }
    printf("@\n");
}
void cq_print(Queue* q)
{
    printf("!~~");
    bool start = true;
    for (Buffer* current = q->head;
            current != NULL && !(start == false && current == q->head);
            current = current->next, start = false)
    {
        printf("> %s --", current->msg);
    }
    printf("@\n");
}

bool is_full(Queue* q) { return q->elements >= QNUM; } // q->elements >= QNUM; }
bool is_empty(Queue* q) { return q->elements <= 0; } // q->elements <= 0; }
bool q_push_back(Queue* q, Buffer* buf)
{
    /* if (q->tail->next != NULL)
       {
       return false;
       }
       */

    if (q->head == NULL)  // implies tail is NULL
    {
        printf("init push\n");
        q->head = buf;
        q->tail = buf;
    }
    else
    {
        printf("inter push\n");
        q->tail->next = buf;
        q->tail = q->tail->next;
    }
    printf("Pushed %s\n", buf->msg);
    q->tail->next = NULL;
    ++(q->elements);
    return true;
}
Buffer* cq_get_next(shm_struct* ptr)
{
    if (is_full(&(ptr->cq)))
    {
        return NULL;
    }
    if (ptr->cq.head == NULL || ptr->cq.tail == &(ptr->buffers[QNUM - 1]))
    {
        return ptr->buffers;
    }
    return ptr->cq.tail + 1;
}
bool cq_push_back(Queue* q, Buffer* buf)
{
    bool value = q_push_back(q, buf);
    if (q->elements == QNUM)
    {
        q->tail->next = q->head;
    }
    return value;
}
Buffer* q_pop(Queue* q)
{
    if (q->head == NULL)  // or q->elements == 0
    {
        return NULL;
    }
    if (q->head == q->tail)
    {
        q->tail = NULL;
        Buffer* temp = q->head;
        q->head = NULL;
        temp->next = NULL;
        --(q->elements);
        return temp;
    }
    else
    {
        Buffer* temp = q->head;
        q->head = q->head->next;
        temp->next = NULL;
        --(q->elements);
        return temp;
    }
}
Buffer* cq_pop(Queue* q)
{
    Buffer* temp = q_pop(q);
    if (q->elements == 0)
    {
        q->head = q->head->next = NULL;
    }
    q->tail->next = NULL;
    return temp;
}
char* q_front(Queue* q)
{
    if (q->head != NULL)
    {
        return q->head->msg;
    }
    return NULL;
}
char* cq_front(Queue* q) { return q_front(q); }

// can't be used with shared memory
bool q_emplace_back(Queue* q, char* msg)
{
    Buffer* tmp = (Buffer*)malloc(sizeof(Buffer*));
    for (int i = 0; i < MSG_SIZE - 1; ++i)
    {
        tmp->msg[i] = msg[i];
    }
    tmp->msg[MSG_SIZE] = '\0';
    tmp->next = NULL;
    return q_push_back(q, tmp);
}

#endif  // _STRUCTS_H_
