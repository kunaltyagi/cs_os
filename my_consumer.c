#include "header.h"
#define TNUM_MAX 4

typedef struct cons_args cons_args;
struct cons_args
{
    sem_t *t_count;
    Buffer *query;
};

void *consumer_thread(void *message)
{
    //Process the query
    int i;
    cons_args *args = (cons_args*)message;
    void *cons_id = (void *)pthread_self();
    printf("\n[Consumer] thread id =%p processing query %s\n", cons_id, args->query->msg);
    
    //CPU time. Reverse the string
    char reverse[MSG_SIZE + 1];
    for(i=0; i<MSG_SIZE; i++)
        reverse[i]=args->query->msg[MSG_SIZE-i];
   
    printf("Reversed string is %s\n", reverse);
/*  File ops is io 
    FILE cons_out = fopen(reverse, "w");
    if(cons_out==NULL)
    {
        printf("Unable to open file\n");
        pthread_exit();
    }
    
    fprintf(cons_out, "%s\n", reverse);
    fclosef(cons_out);
*/

    //Sleep
    usleep(SLEEP_TIME * SEC_FACTOR);
    printf("Thread id = %p completed execution\n", cons_id);
   
    //Free threads
    sem_post(args->t_count); 
    
    return NULL;
          
}

int main()  // int argc, char** argv)
{
    printf("[Consumer] All Begins\n");
    
    Buffer *next;
    shm_struct* shm_mem = (shm_struct*)init_shm_mem("[Producer]");
    Queue *cq = &(shm_mem->cq);

    //Semaphore for managing number of threads
    sem_t thread_num;
    sem_init(&thread_num, 0, TNUM);

    while (shm_mem->cont_sym == '*')
    {
        //Creating a new thread
        sem_wait(&thread_num);
        
        if(pthread_mutex_trylock(&(shm_mem->lock)) != 0)
        {
            while(is_empty(cq))
            {     
		printf("NTA\n");

                pthread_cond_wait(&(shm_mem->buf_full), &(shm_mem->lock));
            }
        }
        pthread_mutex_unlock(&(shm_mem->lock));   
 
        next = cq_get_next(shm_mem);
        cons_args* args = (cons_args*)malloc(sizeof(cons_args));
        args->t_count = &thread_num;
        args->query = next;        
       
        //New thread to handle the query
	pthread_t *thread;
        pthread_create(thread, NULL, consumer_thread, args);             

        usleep(SLEEP_TIME * SEC_FACTOR);
        sleep(2);
        shm_mem->cont_sym = '?';
    }
    end_shm_mem(shm_mem, "[Consumer]");
    printf("[Consumer] All Ends\n");
    return 0;
}
