#ifndef _STRUCTS_H_
#define _STRUCTS_H_

// structs for easier life
typedef struct Buffer Buffer;
struct Buffer
{
    char msg[MSG_SIZE + 1];
    pthread_cond_t buf_process;
    pthread_mutex_t lock;
};
/* const Buffer BUF_EMPTY = { "\0",  NULL, {}, {}}; */

typedef struct Queue Queue;
struct Queue
{
    int head;
    int tail;
};
/* const Queue Q_EMPTY = {0, NULL, NULL}; */

typedef struct shm_struct shm_struct;
struct shm_struct
{
    char cont_sym;
    Queue cq;
    Buffer buffers[QNUM];
    pthread_mutex_t prod_lock, cons_lock, lock;
    pthread_cond_t buf_empty, buf_full;
};
/* const shm_struct MEM_EMPTY = { {}, {}, {}, '\0', {0, NULL, NULL}, {}}; */

bool init_shm_struct(shm_struct* ptr)
{
    pthread_mutexattr_t mutex_attr;
    pthread_mutexattr_init(&mutex_attr);
    pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);

    pthread_condattr_t cond_attr;
    pthread_condattr_init(&cond_attr);
    pthread_condattr_setpshared(&cond_attr, PTHREAD_PROCESS_SHARED);

    if (ptr == NULL)
    {
        return false;
    }
    ptr->cont_sym = '*';

    ptr->cq.head = -1;
    ptr->cq.tail = -1;
    int i;
    for (i = 0; i < QNUM; ++i)
    {
        ptr->buffers[i].msg[0] = '\0';
        /* ptr->buffers[i].next = NULL; */
        pthread_mutex_init(&(ptr->buffers[i].lock), &mutex_attr);
        pthread_cond_init(&(ptr->buffers[i].buf_process), &cond_attr);
    }
    pthread_mutex_init(&(ptr->prod_lock), &mutex_attr);
    pthread_mutex_init(&(ptr->cons_lock), &mutex_attr);
    pthread_mutex_init(&(ptr->lock), &mutex_attr);
    pthread_cond_init(&(ptr->buf_empty), &cond_attr);
    pthread_cond_init(&(ptr->buf_full), &cond_attr);
    return true;
}

void cq_print(shm_struct* mem)
{
    printf("!~~");
    int index;
    for (index = mem->cq.head; index != -1; ++index)
    {
        index = index % QNUM;
        printf("> %s --", mem->buffers[index].msg);
        if (index == mem->cq.tail)
        {
            index = -1;
        }
    }
    printf("@\n");
}

bool is_full(Queue* q) { return ((q->tail + 1) % QNUM == q->head); }
bool is_empty(Queue* q) { return q->head == -1; }
bool q_push_back(Queue* q, Buffer* buf)
{
    if (is_full(q))
    {
        return false;
    }
    if (q->head == -1)
    {
        printf("init push\n");
        q->head = 0;
        q->tail = 0;
    }
    else
    {
        printf("inter push\n");
        q->tail = (q->tail + 1) % QNUM;
    }
    printf("Pushed %s\n", buf->msg);
    return true;
}
Buffer* cq_get_next(shm_struct* ptr)
{
    if (is_full(&(ptr->cq)))
    {
        return NULL;
    }
    if (ptr->cq.head == -1 || ptr->cq.tail == QNUM - 1)
    {
        return ptr->buffers;
    }
    return ptr->buffers + ptr->cq.tail + 1;
}
bool cq_push_back(Queue* q, Buffer* buf)
{
    return q_push_back(q, buf);
}
int q_pop(shm_struct* ptr)
{
    Queue* q = &(ptr->cq);
    if (is_empty(q))
    {
        return -1;
    }
    int num = q->head;
    if (q->head == q->tail)
    {
        q->tail = q->head = -1;
    }
    else
    {
        q->head = (q->head + 1) % QNUM;
    }
    return num;
}
int cq_pop(shm_struct* ptr)
{
    return q_pop(ptr);
}
/* char* q_front(Queue* q) */
/* { */
/*     if (q->head != NULL) */
/*     { */
/*         return q->head->msg; */
/*     } */
/*     return NULL; */
/* } */
/* char* cq_front(Queue* q) { return q_front(q); } */

// can't be used with shared memory
/* bool q_emplace_back(Queue* q, char* msg) */
/* { */
/*     Buffer* tmp = (Buffer*)malloc(sizeof(Buffer*)); */
/*     for (int i = 0; i < MSG_SIZE - 1; ++i) */
/*     { */
/*         tmp->msg[i] = msg[i]; */
/*     } */
/*     tmp->msg[MSG_SIZE] = '\0'; */
/*     tmp->next = NULL; */
/*     return q_push_back(q, tmp); */
/* } */

#endif  // _STRUCTS_H_
