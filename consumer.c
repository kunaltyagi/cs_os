#include "header.h"

void* query_handling(void*);

typedef struct consume_args consume_args;
struct consume_args
{
    Buffer* query;
    sem_t* thread_count;
    pthread_t* thread;
};

int main()  // int argc, char** argv)
{
    printf("[Consumer] All Begins: %d\n", getpid());
    shm_struct* shm_mem = init_shm_mem("[Consumer]", false);

    sem_t thread_num;
    sem_init(&thread_num, 0, TNUM);

    init_shm_struct(shm_mem);
    Queue* cq = &(shm_mem->cq);
    printf("[Consumer] All inits\n");
    pthread_mutex_unlock(&(shm_mem->lock));
    while (shm_mem->cont_sym == '*')
    {
        // Consumer
        printf("[Consumer] Locking buffer\n");
        pthread_mutex_lock(&(shm_mem->lock));
        while (is_empty(cq) == true)
        {
            printf("[Consumer] Buffer empty. Waiting.. \n");
            pthread_cond_wait(&(shm_mem->buf_full), &(shm_mem->lock));
            printf("[Consumer] Signal received \n");
        }
        //pthread_mutex_unlock(&(shm_mem->lock));
        //pthread_mutex_lock(&(shm_mem->lock)); 

        printf("[Consumer] Removing from buffer\n");
        // 1. Take the query and remove it
        Buffer* query = shm_mem->buffers + cq_pop(shm_mem);
        printf("Query is %s\n", query->msg);
        //printf("[Consumer] Unlocking buffer\n");
        
        printf("[Consumer] Sending buffer empty signal\n");
        pthread_cond_signal(&(shm_mem->buf_empty));
        printf("[Consumer] Signal sent\n");
	pthread_mutex_unlock(&(shm_mem->lock));

        pthread_t* thread = (pthread_t*)malloc(sizeof(pthread_t));
        //printf("[Consumer] Allocating memory: %d\n", thread==NULL);
        consume_args* args = (consume_args*)malloc(sizeof(consume_args));
        //printf("[Consumer] Assigning items (1)\n");
        args->query = query;
        //printf("[Consumer] Assigning items (2)\n");
        args->thread_count = &thread_num;
        //printf("[Consumer] Assigning items (3)\n");
        args->thread= thread;
        /* printf("[Consumer] Assigning items (4)\n");
           args->mem = shm_mem; */
        // 0. Create a thread
        sem_wait(&thread_num);
        //printf("[Consumer] Creating thread\n");
        int tid = pthread_create(thread, NULL, query_handling, args);

        printf("[Consumer] Thread created: %d\n\n", tid);
        usleep(SLEEP_TIME * SEC_FACTOR);
	sleep(10);
        //printf("[Consumer] Thread created: %d\n", tid);
        /* usleep(SLEEP_TIME * SEC_FACTOR); */

        // check end
        // shm_mem->cont_sym = '?';
    }
    end_shm_mem("[Consumer]");
    printf("[Consumer] All Ends\n");
    return 0;
}

void* query_handling(void* message)
{
    consume_args* args = (consume_args*)message;
    char origin[128];
    snprintf(origin, sizeof(origin), "[Consumer:%d] ", (unsigned int)pthread_self());

    // 1. print the query, thread_id
    char msg[MSG_SIZE + 1];
    strncpy(msg, args->query->msg, sizeof(msg));
    printf("%sQuery received: %s\n", origin, msg);

    // 2. Consume CPU time
    int i;
    for(i=0; i<1000; i++);

    // 3. Wait
    sleep(SLEEP_TIME);

    // 4. Declare end
    printf("%sCompleted execution\n", origin);

    struct timeval tv;
    char buffer[30];
    time_t curtime;



    // 5. End
    printf("%sSignaling end of query\n", origin);
    gettimeofday(&tv, NULL);
    curtime = tv.tv_sec;
    strftime(buffer,30,"%T",localtime(&curtime));
    fprintf(stderr,"%sMessage: %s, Time :%s\n", origin, msg, buffer);

    //printf("%sLocking query\n", origin);
    pthread_mutex_lock(&(args->query->lock));
    pthread_cond_signal(&(args->query->buf_process));
    //printf("%sUnlocking query\n", origin);
    pthread_mutex_unlock(&(args->query->lock));
    // @TODO: might cause seg fault
    printf("%sDeleting thread\n\n", origin);
    free(args->thread);
    //printf("%sDeleting args\n", origin);
    free(message);
    //printf("%sEnd of thread\n", origin);
    sem_post(args->thread_count);
    return NULL;
}
